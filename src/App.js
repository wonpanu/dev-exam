import { useState } from "react";
import "./App.css";
import axios from "axios";

function App() {
  const [state, setstate] = useState({
    exam: 1,
    tab: 1,
    n: 1,
    result: ["X"],
    result_date: null,
    users: null,
    search: null,
    result_search: null,
  });

  if (state.exam === 1) {
    window.location.replace("/#1");
  }

  const handleSearch = () => {
    console.log("Search", state.search);
    setstate({
      ...state,
      result_search: state.users.filter(
        (i) =>
          i.name &&
          i.username &&
          i.email &&
          (i.name.toLowerCase().includes(state.search.toLowerCase()) ||
            i.username.toLowerCase().includes(state.search.toLowerCase()) ||
            i.email.toLowerCase().includes(state.search.toLowerCase()))
      ),
    });
  };

  const handleChangeN = async (number) => {
    let result = await handleResult(number);
    await setstate({
      ...state,
      n: number,
      result: result,
    });
  };

  const handleResult = (number) => {
    let result = [];
    let str = "";
    if (state.tab === 1) {
      for (let i = 0; i < 2 * number - 1; i++) {
        str = "";
        for (let j = 0; j < 2 * number - 1; j++) {
          if ((i + j) % 2 === 0) {
            if (j === i || j === 2 * number - 2 - i) {
              str += "X";
            } else if (
              (j > i && j < 2 * number - 2 - i) ||
              (i > number - 1 && j >= 2 * number - i && j <= i)
            ) {
              str += "X";
            } else {
              str += "O";
            }
          } else {
            str += "O";
          }
          str += ",";
        }
        result.push(str);
      }
    } else if (state.tab === 2) {
      for (let i = 0; i < number; i++) {
        str = "";
        for (let j = 0; j < number; j++) {
          if (
            (j > i && j < number - 1 - i) ||
            (i > number / 2 && j >= number - i && j < i)
          ) {
            str += "_";
          } else {
            str += "X";
          }
          str += ",";
        }
        result.push(str);
      }
    }

    return result;
  };

  const handleCalculateDate = async (time) => {
    let last_two_digits = time.split("-")[0].slice(2, 4);
    let div = Math.floor(last_two_digits / 4);
    let year_code = (last_two_digits + div) % 7;

    let month = time.split("-")[1];
    let month_code =
      month === "01"
        ? "0"
        : month === "02"
        ? "3"
        : month === "03"
        ? "3"
        : month === "04"
        ? "6"
        : month === "05"
        ? "1"
        : month === "06"
        ? "4"
        : month === "07"
        ? "6"
        : month === "08"
        ? "2"
        : month === "09"
        ? "5"
        : month === "10"
        ? "0"
        : month === "11"
        ? "3"
        : month === "12" && "5";

    let century = time.split("-")[0].slice(0, 2);
    let century_code =
      century === "17"
        ? "4"
        : century === "18"
        ? "2"
        : century === "19"
        ? "0"
        : century === "20"
        ? "6"
        : century === "21"
        ? "4"
        : century === "22"
        ? "2"
        : century === "23" && "0";

    let year = parseInt(time.split("-")[0]);
    let leap_year =
      (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0 ? "1" : "0";

    let date_number = parseInt(time.split("-")[2]);
    let weekday =
      (year_code +
        parseInt(month_code) +
        parseInt(century_code) +
        date_number +
        parseInt(leap_year)) %
      7;

    let weekday_th =
      weekday === 0
        ? "วันอาทิตย์"
        : weekday === 1
        ? "วันจันทร์"
        : weekday === 2
        ? "วันอังคาร"
        : weekday === 3
        ? "วันพุธ"
        : weekday === 4
        ? "วันพฤหัสบดี"
        : weekday === 5
        ? "วันศุกร์​"
        : weekday === 6 && "วันเสาร์";

    let month_th =
      month === "01"
        ? "มกราคม"
        : month === "02"
        ? "กุมภาพันธ์"
        : month === "03"
        ? "มีนาคม"
        : month === "04"
        ? "เมษายน"
        : month === "05"
        ? "พฤษภาคม"
        : month === "06"
        ? "มิถุนายน"
        : month === "07"
        ? "กรกฎาคม"
        : month === "08"
        ? "สิงหาคม"
        : month === "09"
        ? "กันยายน"
        : month === "10"
        ? "ตุลาคม"
        : month === "11"
        ? "พฤศจิกายน"
        : month === "12" && "ธันวาคม";

    let result = await (weekday_th +
      "ที่ " +
      time.split("-")[2] +
      " " +
      month_th +
      " " +
      (parseInt(year) + 543).toString());

    await setstate({
      ...state,
      result_date: result,
    });
  };

  return (
    <div className="App">
      <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
        <div className="container-fluid">
          <a className="navbar-brand mb-0 h1" href="#1">
            <strong>Bitkub Developer Exam</strong>
          </a>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li
                className="nav-item"
                onClick={() => setstate({ ...state, exam: 1 })}
              >
                <a
                  className={`nav-link${state.exam === 1 ? " active" : ""}`}
                  href="#1"
                >
                  #1
                </a>
              </li>
              <li
                className="nav-item"
                onClick={() => setstate({ ...state, exam: 2 })}
              >
                <a
                  className={`nav-link${state.exam === 2 ? " active" : ""}`}
                  href="#2"
                >
                  #2
                </a>
              </li>
              <li
                className="nav-item"
                onClick={() => {
                  !state.users
                    ? axios
                        .get("https://jsonplaceholder.typicode.com/users")
                        .then((res) => {
                          console.log("Users", res.data);
                          res &&
                            setstate({
                              ...state,
                              users: res.data,
                              result_search: res.data,
                              exam: 3,
                              search: null,
                            });
                        })
                        .catch((err) => console.log(err))
                    : setstate({
                        ...state,
                        exam: 3,
                        search: null,
                      });
                }}
              >
                <a
                  className={`nav-link${state.exam === 3 ? " active" : ""}`}
                  href="#3"
                >
                  #3
                </a>
              </li>
            </ul>
            {state.exam === 3 && (
              <form className="d-flex">
                <input
                  className="form-control me-2"
                  type="search"
                  placeholder="Search"
                  aria-label="Search"
                  value={state.search}
                  onChange={(e) =>
                    setstate({ ...state, search: e.target.value })
                  }
                />
                <button
                  className="btn btn-outline-light"
                  type="button"
                  onClick={() => handleSearch()}
                >
                  Search
                </button>
              </form>
            )}
          </div>
        </div>
      </nav>
      {state.exam === 1 ? (
        <>
          <nav>
            <div className="nav nav-tabs" id="nav-tab" role="tablist">
              <a
                className="nav-link active"
                id="nav-pattern-1-tab"
                data-bs-toggle="tab"
                href="#pattern-1"
                role="tab"
                aria-controls="nav-pattern-1"
                aria-selected="true"
                onClick={() => setstate({ ...state, tab: 1, n: 1 })}
              >
                Pattern 1.1
              </a>
              <a
                className="nav-link"
                id="nav-pattern-2-tab"
                data-bs-toggle="tab"
                href="#pattern-2"
                role="tab"
                aria-controls="nav-pattern-2"
                aria-selected="false"
                onClick={() =>
                  setstate({ ...state, tab: 2, n: 1, result: ["X"] })
                }
              >
                Pattern 1.2
              </a>
              <a
                className="nav-link"
                id="nav-pattern-3-tab"
                data-bs-toggle="tab"
                href="#pattern-3"
                role="tab"
                aria-controls="nav-pattern-3"
                aria-selected="false"
                onClick={() => setstate({ ...state, tab: 3, n: 1 })}
              >
                Pattern 1.3
              </a>
            </div>
          </nav>
          <table className="container-fluid" style={{ height: "90vh" }}>
            <tbody>
              <tr>
                <td className="align-middle">
                  <div className="tab-content" id="nav-tabContent">
                    <div
                      className={`tab-pane fade${
                        state.tab === 1 ? " show active" : ""
                      }`}
                      id="nav-pattern-1"
                      role="tabpanel"
                      aria-labelledby="nav-pattern-1-tab"
                      style={{
                        display: state.tab === 1 ? "flex" : "none",
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      <div
                        className="card"
                        style={{
                          minWidth: "520px",
                          minHeight: "520px",
                          marginTop: "0%",
                        }}
                      >
                        <h5 className="card-header">
                          <strong>Pattern 1.1</strong>
                        </h5>
                        <div
                          className="card-body"
                          style={{ padding: "5%", textAlign: "left" }}
                        >
                          <h5 className="card-title">
                            Please input{" "}
                            <strong style={{ fontSize: "24px" }}>N</strong>{" "}
                            number
                          </h5>
                          <div
                            className="input-group mb-3"
                            style={{ maxWidth: "120px" }}
                          >
                            <span
                              className="input-group-text"
                              id="inputGroup-sizing-default"
                            >
                              Input
                            </span>
                            <input
                              type="number"
                              value={state.n}
                              className="form-control"
                              aria-label="Sizing example input"
                              aria-describedby="inputGroup-sizing-default"
                              onChange={(e) => handleChangeN(e.target.value)}
                            />
                          </div>
                          <h5 className="card-text" style={{ marginTop: "5%" }}>
                            <strong>Output</strong>
                          </h5>
                          <tbody>
                            {state.result?.map((value, index) => (
                              <tr key={index}>
                                {value.split(",").map((i) => (
                                  <td style={{ fontSize: "10px" }}>{i}</td>
                                ))}
                              </tr>
                            ))}
                          </tbody>
                        </div>
                      </div>
                    </div>
                    <div
                      className={`tab-pane fade${
                        state.tab === 2 ? " show active" : ""
                      }`}
                      id="nav-pattern-2"
                      role="tabpanel"
                      aria-labelledby="nav-pattern-2-tab"
                      style={{
                        display: state.tab === 2 ? "flex" : "none",
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      <div
                        className="card"
                        style={{
                          minWidth: "520px",
                          minHeight: "520px",
                          marginTop: "0%",
                        }}
                      >
                        <h5 className="card-header">
                          <strong>Pattern 1.2</strong>
                        </h5>
                        <div
                          className="card-body"
                          style={{ padding: "5%", textAlign: "left" }}
                        >
                          <h5 className="card-title">
                            Please input{" "}
                            <strong style={{ fontSize: "24px" }}>N</strong>{" "}
                            number
                          </h5>
                          <div
                            className="input-group mb-3"
                            style={{ maxWidth: "120px" }}
                          >
                            <span
                              className="input-group-text"
                              id="inputGroup-sizing-default"
                            >
                              Input
                            </span>
                            <input
                              type="number"
                              value={state.n}
                              className="form-control"
                              aria-label="Sizing example input"
                              aria-describedby="inputGroup-sizing-default"
                              onChange={(e) => handleChangeN(e.target.value)}
                            />
                          </div>
                          <h5 className="card-text" style={{ marginTop: "5%" }}>
                            <strong>Output</strong>
                          </h5>
                          <tbody>
                            {state.result?.map((value, index) => (
                              <tr key={index}>
                                {value.split(",").map((i) => (
                                  <td style={{ fontSize: "10px" }}>
                                    {i === "_" ? " " : i}
                                  </td>
                                ))}
                              </tr>
                            ))}
                          </tbody>
                        </div>
                      </div>
                    </div>
                    <div
                      className={`tab-pane fade${
                        state.tab === 3 ? " show active" : ""
                      }`}
                      id="nav-pattern-3"
                      role="tabpanel"
                      aria-labelledby="nav-pattern-3-tab"
                      style={{
                        display: state.tab === 3 ? "flex" : "none",
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      <div
                        className="card"
                        style={{
                          minWidth: "520px",
                          minHeight: "520px",
                          marginTop: "0%",
                        }}
                      >
                        <h5 className="card-header">
                          <strong>Pattern 1.3</strong>
                        </h5>
                        <div
                          className="card-body"
                          style={{ padding: "5%", textAlign: "left" }}
                        >
                          <h5 className="card-title">
                            Please input{" "}
                            <strong style={{ fontSize: "24px" }}>N</strong>{" "}
                            number
                          </h5>
                          <div
                            className="input-group mb-3"
                            style={{ maxWidth: "120px" }}
                          >
                            <span
                              className="input-group-text"
                              id="inputGroup-sizing-default"
                            >
                              Input
                            </span>
                            <input
                              type="number"
                              value={state.n}
                              className="form-control"
                              aria-label="Sizing example input"
                              aria-describedby="inputGroup-sizing-default"
                              onChange={(e) => handleChangeN(e.target.value)}
                              disabled
                            />
                          </div>
                          <h5 className="card-text" style={{ marginTop: "5%" }}>
                            <strong>Output</strong>
                          </h5>
                          <div style={{ color: "red" }}>ยังไม่ได้ทำครับ</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </>
      ) : state.exam === 2 ? (
        <table className="container-fluid" style={{ height: "90vh" }}>
          <tbody>
            <tr>
              <td className="align-middle">
                <div className="tab-content" id="nav-tabContent">
                  <div
                    className="tab-pane fade show active"
                    id="nav-ex2"
                    role="tabpanel"
                    aria-labelledby="nav-ex2-tab"
                    style={{
                      display: state.tab === 1 ? "flex" : "none",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <div
                      className="card"
                      style={{
                        minWidth: "520px",
                        minHeight: "520px",
                        marginTop: "0%",
                      }}
                    >
                      <h5 className="card-header">
                        <strong>Calculate Date</strong>
                      </h5>
                      <div
                        className="card-body"
                        style={{ padding: "5%", textAlign: "left" }}
                      >
                        <h5 className="card-title">
                          Please input{" "}
                          <strong style={{ fontSize: "24px" }}>Datetime</strong>{" "}
                          <span style={{ color: "GrayText" }}>
                            (DD/MM/YYYY)
                          </span>
                        </h5>
                        <div
                          className="input-group mb-3"
                          style={{ maxWidth: "300px" }}
                        >
                          <span
                            className="input-group-text"
                            id="inputGroup-sizing-default"
                          >
                            Datetime
                          </span>
                          <input
                            type="date"
                            className="form-control"
                            aria-label="Sizing example input"
                            aria-describedby="inputGroup-sizing-default"
                            onChange={(e) =>
                              handleCalculateDate(e.target.value)
                            }
                          />
                        </div>
                        <h5 className="card-text" style={{ marginTop: "5%" }}>
                          <strong>Output</strong>
                        </h5>
                        <span
                          style={{ color: state.result_date ? "blue" : "red" }}
                        >
                          {state.result_date
                            ? state.result_date
                            : "Please input date"}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      ) : (
        state.exam === 3 && (
          <table className="container-fluid" style={{ height: "90vh" }}>
            <tbody>
              <tr>
                <td className="align-middle">
                  <div className="tab-content" id="nav-tabContent">
                    <div
                      className="tab-pane fade show active"
                      id="nav-ex2"
                      role="tabpanel"
                      aria-labelledby="nav-ex2-tab"
                      style={{
                        display: state.tab === 1 ? "flex" : "none",
                        alignItems: "start",
                        justifyContent: "start",
                      }}
                    >
                      <div
                        className="row"
                        style={{ paddingLeft: "20px", paddingRight: "20px" }}
                      >
                        {state.result_search &&
                          state.result_search.map((value, index) => (
                            <div
                              className="col-xl-3 col-md-4 col-sm-6"
                              style={{ marginTop: "20px" }}
                            >
                              <div
                                key={index}
                                className="card"
                                style={{
                                  minWidth: "320px",
                                  minHeight: "360px",
                                }}
                              >
                                <h5 className="card-header">
                                  <strong>{value.name}</strong>
                                </h5>
                                <div
                                  className="card-body"
                                  style={{ textAlign: "left" }}
                                >
                                  <div>
                                    <span>
                                      <strong>Username: </strong>
                                    </span>
                                    <span>{value.username}</span>
                                  </div>
                                  <div>
                                    <span>
                                      <strong>Email: </strong>
                                    </span>
                                    <span>{value.email}</span>
                                  </div>
                                  <div>
                                    <span>
                                      <strong>Address: </strong>
                                    </span>
                                    <span>{`${value.address?.street}, ${value.address.suite}, ${value.address.city}, ${value.address.zipcode} (${value.address.geo.lat}, ${value.address.geo.lng})`}</span>
                                  </div>
                                  <div>
                                    <span>
                                      <strong>Phone: </strong>
                                    </span>
                                    <span>{value.phone}</span>
                                  </div>
                                  <div>
                                    <span>
                                      <strong>Website: </strong>
                                    </span>
                                    <span>{value.website}</span>
                                  </div>
                                  <div>
                                    <span>
                                      <strong>Company: </strong>
                                    </span>
                                    <div>- {value.company.name}</div>
                                    <div>- {value.company.catchPhrase}</div>
                                    <div>- {value.company.bs}</div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          ))}
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        )
      )}
    </div>
  );
}

export default App;
